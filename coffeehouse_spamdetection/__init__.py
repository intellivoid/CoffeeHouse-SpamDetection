from . import main
from .main import *

from . import server
from .server import *

__all__ = ["main", "SpamDetection", "Server"]